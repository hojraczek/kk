import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;

/**
 * Created by xxx on 2016-05-18.
 */
public class LoadImagePanel{
    private JPanel panel2;
    private JTextPane wczytajObrazekTextPane;
    private JRadioButton radioButton1;
    private JButton button1;
    private JTextArea textArea1;
    private JRadioButton radioButton2;
    private JTextField URLTextField;
    private JButton okButton;
    private JButton anulujButton;


    static public void show()
    {
        JFrame frame = new JFrame("LoadImagePanel");
        frame.setContentPane(new LoadImagePanel().panel2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    public LoadImagePanel() {

        radioButton1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                button1.setEnabled(e.getStateChange() == ItemEvent.SELECTED);
                textArea1.setEnabled(e.getStateChange() == ItemEvent.SELECTED);
            }
        });
        radioButton2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
            URLTextField.setEnabled(e.getStateChange() == ItemEvent.SELECTED);
            }
        });

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser("/Users/xxx/IdeaProjects/asciiart/ASCIIArt/src/");
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "PGM Images", "pgm");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showOpenDialog(textArea1.getParent());
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    textArea1.setText(chooser.getSelectedFile().getAbsolutePath());
                }
            }
        });
        anulujButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        });
    }


}

